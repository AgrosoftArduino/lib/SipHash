#include "SipHash_2_4.h"

const uint8_t key[] PROGMEM = "0123456789ABCDEF";

char msg[] = "Hello world!";

void setup() {
    Serial.begin(115200);
}

void loop() {
    unsigned long startTime;
    unsigned long stopTime;

    Serial.println();
    Serial.println();
    Serial.println(F("SipHashTest with init"));
    delay(200);


    Serial.print(F("Msg: "));
    Serial.println(msg);

    startTime = micros();

    sipHash.initFromPROGMEM(key);

    sipHash.updateHashStr(msg);
    
    sipHash.finish();

    stopTime = micros();

    Serial.print(F("sipHash: "));

    for (uint8_t i = 0; i < 8; i++) {
        char s[3];
        sprintf(s, "%02x", sipHash.result[i]);
        Serial.print(s);
    }
    Serial.println();

    Serial.print(F("Running time: "));
    Serial.print((stopTime - startTime) / 1000.0);
    Serial.println(F(" ms"));

    delay(2000);
}
