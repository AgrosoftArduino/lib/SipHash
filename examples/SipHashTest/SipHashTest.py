#!/usr/bin/env python3

import siphash

key = '0123456789ABCDEF'
msg = 'Hello world!'

print('SipHashTest')
print('Msg:', msg)
sip = siphash.SipHash_2_4(key.encode())
sip.update(msg.encode())
print('sipHash:', hex(sip.hash()))
